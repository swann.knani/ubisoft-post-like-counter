<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Ubisoft_Post_Like_Counter
 * @subpackage Ubisoft_Post_Like_Counter/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ubisoft_Post_Like_Counter
 * @subpackage Ubisoft_Post_Like_Counter/public
 * @author     Swann Knani <swann.knani@gmail.com>
 */
class Ubisoft_Post_Like_Counter_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $ubisoft_post_like_counter    The ID of this plugin.
	 */
	private $ubisoft_post_like_counter;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $option_name    The option name used to store post likes.
	 */
	private $option_name;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param string $ubisoft_post_like_counter The name of this plugin.
	 * @param string $version                   The version of this plugin.
	 * @param string $option_name               The option name used to store post likes.
	 */
	public function __construct( $ubisoft_post_like_counter, $version, $option_name ) {

		$this->ubisoft_post_like_counter = $ubisoft_post_like_counter;
		$this->version                   = $version;
		$this->option_name               = $option_name;

		// $this->get_most_liked_compostion_posts(3);
	}
	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		* This function is provided for demonstration purposes only.
		*
		* An instance of this class should be passed to the run() function
		* defined in Ubisoft_Post_Like_Counter_Loader as all of the hooks are defined
		* in that particular class.
		*
		* The Ubisoft_Post_Like_Counter_Loader will then create the relationship
		* between the defined hooks and the functions defined in this
		* class.
		*/

		wp_enqueue_script( $this->ubisoft_post_like_counter, plugin_dir_url( __FILE__ ) . 'js/ubisoft-post-like-counter-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Localize the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function localize_script() {

		wp_localize_script( $this->ubisoft_post_like_counter, 'settings', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}

	private function get_post_like_count_since_elapsed_time( $post_id, $post_likes ) {

		if ( ! array_key_exists( $post_id, $post_likes ) ) {

			return 0;

		}

		return count( $post_likes[ $post_id ] );

	}

	private function update_post_like_count( $post_id ) {

		$like_count = (int) get_field( 'like_count', $post_id ) ?? 0;
		$like_count++;

		update_field( 'like_count', $like_count, $post_id );

		return $like_count;
	}

	private function update_likes_since_elapsed_time( $post_id ) {

		$likes_since_elapsed_time = $this->get_like_count_since_elapsed_time();

		$now = time();

		if ( array_key_exists( $post_id, $likes_since_elapsed_time ) ) {

			$likes_since_elapsed_time[ $post_id ][] = $now;

		} else {

			$likes_since_elapsed_time[ $post_id ] = array( $now );
		}

		update_option( $this->option_name, $likes_since_elapsed_time, false );

		return $likes_since_elapsed_time;
	}

	public function get_most_liked_post_ids() {

		$likes_since_elapsed_time = $this->get_like_count_since_elapsed_time();

		uasort($likes_since_elapsed_time, function ( $a, $b ) {

			return count( $b ) - count( $a );
		});

		return array_keys( $likes_since_elapsed_time );

	}

	private function get_like_count_since_elapsed_time() {

		$likes_since_elapsed_time = get_option( $this->option_name ) ?: array();

		return is_array( $likes_since_elapsed_time ) ? $likes_since_elapsed_time : array();

	}

	public function increment_like_count() {

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

			if ( isset( $_REQUEST['post_id'] ) && absint( $_REQUEST['post_id'] ) ) { // Input var okay.

				$post_id = $_REQUEST['post_id'];
				$post    = get_post( $post_id );

				$has_like_capability = $post && in_array( $post->post_type, LIKE_CAPABILITIES, true );

				if ( $has_like_capability ) {

					// Like count.
					$like_count = $this->update_post_like_count( $post_id );

					// Like count since elapsed time.
					$likes_since_elapsed_time = $this->update_likes_since_elapsed_time( $post_id );

					// Post like count since elapsed time.
					$post_like_count_since_elapsed_time = $this->get_post_like_count_since_elapsed_time( $post_id, $likes_since_elapsed_time );

					update_post_meta( $post_id, $this->option_name, $post_like_count_since_elapsed_time );

					wp_send_json(array(
						'success'    => true,
						'like_count' => $like_count,
					));

				} else {

					wp_send_json_error();
				}
			}
		}

		die();
	}
}
