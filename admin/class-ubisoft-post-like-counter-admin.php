<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Ubisoft_Post_Like_Counter
 * @subpackage Ubisoft_Post_Like_Counter/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ubisoft_Post_Like_Counter
 * @subpackage Ubisoft_Post_Like_Counter/admin
 * @author     Swann Knani <swann.knani@gmail.com>
 */
class Ubisoft_Post_Like_Counter_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $ubisoft_post_like_counter    The ID of this plugin.
	 */
	private $ubisoft_post_like_counter;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $option_name    The option name used to store post likes.
	 */
	private $option_name;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param string $ubisoft_post_like_counter The name of this plugin.
	 * @param string $version                   The version of this plugin.
	 * @param string $option_name               The option name used to store post likes.
	 */
	public function __construct( $ubisoft_post_like_counter, $version, $option_name ) {

		$this->ubisoft_post_like_counter = $ubisoft_post_like_counter;
		$this->version                   = $version;
		$this->option_name               = $option_name;

		$this->create_options();
		$this->update_posts_like_count_since_elapsed_time();

	}

	/**
	 * Initialize option name for storing post likes.
	 *
	 * @since    1.0.0
	 */
	private function create_options() {

		add_option( $this->option_name, array(), '', false );
	}

	/**
	 * Cron to update posts like count.
	 *
	 * @since    1.0.0
	 */
	public function update_posts_like_count_since_elapsed_time() {

		$top_compositions_period = intval( get_theme_mod( 'top_compositions_period', 30 ) );
		$exclude_before          = time() - ( $top_compositions_period * 24 * 60 * 60 );

		$likes_since_elapsed_time = get_option( $this->option_name ) ?: array();

		$likes_since_elapsed_time = is_array( $likes_since_elapsed_time ) ? $likes_since_elapsed_time : array();

		$posts_likes_since_elapsed_time = array_map(function( $arg ) use ( $top_compositions_period, $exclude_before ) {
			return array_filter($arg, function( $val ) use ( $exclude_before ) {
				return $val > $exclude_before;
			});
		}, $likes_since_elapsed_time );

		$result = array_filter( $posts_likes_since_elapsed_time, 'count' );

		update_option( $this->option_name, $result, false );
	}

}
