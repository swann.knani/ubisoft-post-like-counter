<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Ubisoft_Post_Like_Counter
 * @subpackage Ubisoft_Post_Like_Counter/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ubisoft_Post_Like_Counter
 * @subpackage Ubisoft_Post_Like_Counter/includes
 * @author     Swann Knani <swann.knani@gmail.com>
 */
class Ubisoft_Post_Like_Counter_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		if ( ! wp_next_scheduled( 'ubisoft_post_like_counter_cron_hook' ) ) {

			wp_schedule_event( time(), 'hourly', 'cron_update_posts_like_count_since_elapsed_time' );
		}

	}
}
