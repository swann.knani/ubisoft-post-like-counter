<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Ubisoft_Post_Like_Counter
 * @subpackage Ubisoft_Post_Like_Counter/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ubisoft_Post_Like_Counter
 * @subpackage Ubisoft_Post_Like_Counter/includes
 * @author     Swann Knani <swann.knani@gmail.com>
 */
class Ubisoft_Post_Like_Counter_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		$timestamp = wp_next_scheduled( 'ubisoft_post_like_counter_cron_hook' );
		
		wp_unschedule_event( $timestamp, 'ubisoft_post_like_counter_cron_hook' );
	}

}
